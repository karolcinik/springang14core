package com.springtemplate.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.springtemplate.core"})
public class SpringAng14Core {

	public static void main(String[] args) {
		SpringApplication.run(SpringAng14Core.class, args);
	}
}
