package com.springtemplate.core;


import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc
@Configuration
@AutoConfigureAfter(DispatcherServletAutoConfiguration.class)
public class StaticContentConfiguration extends WebMvcConfigurerAdapter {

  private static final Logger LOG = LoggerFactory.getLogger(StaticContentConfiguration.class);
  
  private static final String LOCATION_TYPE = "file://";
  private static final String SERVER_RESOURCE_PATH = "/**";
  private static final String INDEX_NAME = "index.html";
  private static final String VIEW_STATIC_CONTENT = "forward:/" + INDEX_NAME;
  private static final String URL_STATIC_CONTENT = "/";
  
  @Autowired
  WebConfigProperties webConfigProperties;

  @Bean
  public InternalResourceViewResolver defaultViewResolver() {
    return new InternalResourceViewResolver();
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(SERVER_RESOURCE_PATH).addResourceLocations(getAbsoluteResourceFilePath());
    logAbsoluteLocationPath();
    super.addResourceHandlers(registry);
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController(URL_STATIC_CONTENT).setViewName(VIEW_STATIC_CONTENT);
    registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    super.addViewControllers(registry);
  }

  private void logAbsoluteLocationPath() {
    LOG.info("relative project path  : " + webConfigProperties.getStaticContentLocation());
    LOG.info("absolute project path  : " + webConfigProperties.getUserDirPath());
    LOG.info("absolute resource path : " + getAbsoluteResourceFilePath());
  }
  
  private String getAbsoluteResourceFilePath() {
    Path relativePath = Paths.get(webConfigProperties.getStaticContentLocation());
    Path projectAbsolutePath = Paths.get(webConfigProperties.getUserDirPath()).toAbsolutePath();
    return LOCATION_TYPE + projectAbsolutePath.resolve(relativePath).normalize().toAbsolutePath().toUri().getPath();
  }
}