package com.springtemplate.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class WebConfigProperties {
  
  private static final String USER_DIR_PROPERTY = "user.dir";
  private static final String STATIC_CONTENT_LOCATION = "static.content.location";
  
  @Autowired
  private Environment environment;

  public String getStaticContentLocation() {
    return environment.getProperty(STATIC_CONTENT_LOCATION);
  }

  public String getUserDirPath() {
    return System.getProperty(USER_DIR_PROPERTY);
  }
}
